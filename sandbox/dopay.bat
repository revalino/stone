@echo off
if "x%1" == "x" (
   echo usage: dopay amount cardId
   echo usage: dopay 150.50 1
) else (
   curl -i -X POST -H "Content-Type: application/json" -b cookies.txt %url%%ctx%/api/invoice/%1/pay/%2
)
@echo off
if "x%1" == "x" (
   echo usage: addWallet email password limit
   echo example: addWallet someuser@site.com 1@33 1500
) else (
	curl -i -H "Content-Type: application/json" -X POST -d {\"email\":\"%1\",\"password\":\"%2\",\"limit\":\"%3\"} %url%%ctx%/public/wallet/
)
@echo off	
if [%1] == [] (
   echo usage: newPurchase when value description
   echo example: newPurchase 28-11-2019 11:30 340.00 Ali
) else (
   @echo on
   curl -i -H "Content-Type: application/json" -b cookies.txt -X POST -d {\"when\":\"%1\",\"value\":\"%2\",\"description\":\"%3\"} %url%%ctx%/api/purchase
)
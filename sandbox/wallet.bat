@echo off
if "x%1" == "x" (
   echo usage: wallet id. For currently logged in user, use 0 as id.
) else (
   curl -i --header "Accept:application/json" -b cookies.txt %url%%ctx%/api/wallet/%1
)
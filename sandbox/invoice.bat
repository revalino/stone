@echo off
if "x%1" == "x" (
   echo usage: invoice cardId MMyyyy
   echo example: invoice 1 122017
) else (
	curl -i -H "Content-Type: application/json" -b cookies.txt  %url%%ctx%/api/invoice/%1/%2
)
@echo off
if "x%1" == "x" (
   echo usage: addCard number dueDay namePrint goodThru limit cvv
   echo example: addCard 1112999844432225 15 "Name SurName" 1123 1800 666
) else (
	curl -i -H "Content-Type: application/json" -b cookies.txt -X POST -d {\"number\":\"%1\",\"dueDay\":\"%2\",\"namePrint\":\"%3\",\"goodThru\":\"%4\",\"limit\":\"%5\",\"cvv\":\"%6\"} %url%%ctx%/api/card
)
# Stone Wallet

A REST API implementation designed to create and manage a virtual Wallet of credit cards. The user can create, add cards, define limits, make purchases and pay card invoices. This API will automatically split purchases between cards according to their due dates and individual limits. As long a payment is made the overall wallet's limit grows back. 

## Getting Started

In order to make it up and running, just build it from source using Maven. Then, deploy it on a JEE Application Server such as WildFly. This application uses MySQL as Permanent storage. This project contains SQL scripts to create the database with all mandatory objects this applications relies upon.

### Prerequisites

* MySQL Server 5.5 or later
* Java 8 or later
* WildFly 10.1 or later
* Apache Maven 3.3 or later
* Git client

### Installing

Here follows a step by step guide to make you having a development env up and running. Here we're assuming having a Windows Machine. Translate the commands to a Linux environment is straightforward. 

Clone repository .

```
git clone  https://revalino@bitbucket.org/revalino/stone.git
```

Install database into mysql. 
The script to create all tables can be found on directory **ddl** inside project's directory.
Assumming *mysql* client is PATH:

```
mysql -u root -p
create database stone;
use stone;
source ddl\ddls.txt
exit
``` 

Configure Database Connection into Application.
Open *pom.xml* and search the following entries to setup database credentials:

```xml 
    <jdbc.user>YOUR_MYSQL_USER</jdbc.user>
    <jdbc.password>YOUR_MYSQL_PWD</jdbc.password>
``` 

Build application from source code.

```
mvn clean package -Dmaven.test.skip=true
```

Deploy generated artifact into JEE Application Server. In this case we'll use $WILDFLY point to a valid WildFly 10.1 installation directory.

```
copy target\wallet-endpoint.war $WILDFLY\standalone\deployments
copy con $WILDFLY\standalone\deployments\wallet-endpoint.war.dodeploy
CTRL+Z
```

Start JEE Application Server.

```
cd $WILDFLY\bin
run.bat -b 0.0.0.0
```

## Running the tests

In order to test the API you can use two approaches:

### Test Using CURL

Curl is a console HTTP Client application. You can specify Headers, supply cookies, define HTTP Methods, payloads and so forth. With Curl you can verify the API, instrumenting the Requested Body or Path, and checking the Response.

We've built some Windows Bat scripts to make it easier testing the API. With these scripts you can cover most of features this API comes with. Basically you can:

* Create a wallet
* Login
* Add Cards to the wallet
* View Cards
* Buy things
* View Purchases 
* View card invoices
* Pay Cards
* Logout

The scripts are kept in _sandobox_ directory. All BAT files have a guide teaching the expected parameters. So, to use any script, just run it, see how the correct way to invoke it and try it again. Ex:
 
```
sandbox\addWallet.bat
usage: addWallet email password limit
example: addWallet someuser@site.com 1@33 1500
```

### Test Using SWAGGER

Another way to test is through Swagger framework. It's basically a toolset built upon Spring Framework and compatible with JUnit to make TestCases. Using your preferred Java IDE, you can just Open the class file *LiveTest* and run it using JUnit.

In this class you can see many methods interacting with a live server - you define the URL in the beginning of the class. JUnit will call *fullCycle* method and, programmatically you can check some API calls. 

You need to switch on WildFly before start testing.

## API Details

This sections describes all URIs served by this API.

###Public (Unauthenticated) URIs

```
###/public/wallet
```

path|method|parameter|response|Detail
----|------|---------|--------|------
/public/wallet|POST|Wallet JSON|HTTP 200 or 403|Create new Wallet. Don't need to be authenticated.

###Private (Authenticated) URIs

```
/api/wallet
```

path|method|parameter|response|Detail
----|------|---------|--------|------
/api/wallet/{id}|GET|wallet id|Wallet JSON|Shows Wallet's data
/api/wallet|PUT|Wallet JSON|Wallet JSON|Updates Wallet data

```
/admin/wallet
```
path|method|parameter|response|Detail
----|------|---------|--------|------
/admin/wallet/{id}|DELETE|wallet id|HTTP 200 or 404|Only admin users can issue this command. It remove all Wallet data.

```
/api/card
```
path|method|parameter|response|Detail
----|------|---------|--------|------
/api/card/{id}|GET|card id|Card JSON or HTTP 204|Shows Card's data
/api/card/{id}|DELETE|card id|HTTP 200 or 401|Remove all Card Information
/api/card|GET||List of Cards JSON or HTTP 204|List all Cards current user holds
/api/card|POST|Card JSON|Card JSON|Create a new Card into Wallet

```
/api/purchase
```
path|method|parameter|response|Detail
----|------|---------|--------|------
/api/purchase/{id}|GET|purchase id|Purchase JSON or HTTP 204|Show Purchase data
/api/purchase/{id}|DELETE|purchase id|HTTP 200 or 401|Remove this Purchase information
/api/purchase/period/{mmyyyy}|GET|List of Purchases JSON|List all Purchases for a given month/year
/api/purchase|POST|Purchase JSON|Purchase JSON or HTTP 406|Make new Purchase. Automatically span value across cards if necessary. Wallet and cards limit shrinks down.

```
/api/invoice
```
path|method|parameter|response|Detail
----|------|---------|--------|------
/api/invoice/{idcard}/{mmyyyy}|GET|card id and month/year|Invoice JSON, HTTP 403 or HTTP 204|List Invoice for given month/year and given card id
/api/invoice/{idcard}/last|GET|card id|Invoice JSON or HTTP 403|Retrieve last closed Invoice
/api/invoice/{idcard}/open|GET|card id|Invoice JSON or HTTP 403|Retrieve last opened Invoice (Still accepts purchases)
/api/invoice/{idcard}/pay|POST|card id|HTTP 201 or HTTP 403|Pay full invoice's value. Wallet and card's limits grows back again
/api/invoice/{idcard}/pay/{amount}|POST/card id and amount to pay|HTTP 201 or HTTP 403|Pay partial invoice's value. Wallet and card's limits grows back again

##JSON Formats

###Wallet

```json
{
	 "email":"credentials@email.com",
	 "password":"your_strong_password",
	 "limit":"100.00",
	 "maxLimit":"500.00",
	 "available":"30.15",
	 listCards: []
}
```

* email: Any valid email (API don't validate email format so far)
* password: Any password (API don't validate password strength so far)
* limit: can be ignored, if so limit will be the sum of each card's limit. API uses dot '.' as cents separator
* maxLimit: Sum of all card's limit. Don't provide it when PUT Json to API.
* available: Limit minus Sum of all Purchases. Don't provide it when PUT Json to API.
* listCards: An array of Cards JSON. Don't provide it when PUT Json to API.

###Card

```json
{
	 "number":"1111222233334444",
	 "dueDay":"10",
	 "namePrint":"Owner Name as Printed",
	 "goodThru":"MMYY",
	 "limit":"600.00",
	 "available":"100.00",
	 "cvv":"111"
}
```

* number: Any valid card number. (API don't validate card number so far)
* dueDay: A month's day number when purchases needs to be payed. (Invoice is closed 10 days before that day)
* namePrint: Any valid human being/company name.
* goodThru: A four digit string with month and year when this cards is no longer valid (API don't validate this indeed)
* limit: Card's limit. API uses dot '.' as cents separator
* available: Limit minus Sum of Purchases for this Card. Don't provide it when PUT Json to API. 
* cvv: Any valid set of digits. (API don't validate this)

###Purchase

```json
{
	"when":"15-01-2017 10:56",
	"value":"27.55",
	"description":"Snack at Barney's pub",
	"listDetails":[]
}
```

* when: A 'dd-MM-yyyy HH:mm' string. API will accept only dates using this format.
* value: The purchase value. API uses dot '.' as cents separator.
* description: The purchase description.
* listDetails: An array of PurchaseDetails. This structure holds how purchase have scattered values between Wallet's cards. Don't provide it when PUT Json to API.
 

###PurchaseDetails
```json
{
	"card":"{}",
	"value":"27.55"
}
```

* card: Card JSON
* value: The value this card payed in this transaction 

###Invoice

```json
{
	"dueDate":"18",
	"beginPeriod":"12-12-2017",
	"endPeriod":"12-01-2018",
	"total":"257.48",
	"payment":"257.48",
	"listPayments":"[]"
}
```
dueDate: The month's day, after endPeriod where this invoice needs to get payed
beginPeriod: A 'dd-MM-yyyy' string. API will accept only dates using this format.
endPeriod: A 'dd-MM-yyyy' string. API will accept only dates using this format.
total: Invoice total value. API uses dot '.' as cents separator.
payment: Invoice total payed. uses dot '.' as cents separator.
listPayments: An array of InvoiceClearance JSON. InvoiceClearance allows API to register more than one payment per Invoice.

####InvoiceClearance
```json
{
	"amount":"27.55"
	"when":"04-01-2018",
}
```

* amount: The money used to pay this bill. API uses dot '.' as cents separator.
* when: A 'dd-MM-yyyy' string. API will accept only dates using this format.

# Built With

* [SpringFramework](https://spring.io/guides/gs/rest-service/) - The web framework used
* [Hibernate](http://hibernate.org/orm/documentation/5.0/) - The persistence framework used
* [Maven](https://maven.apache.org/) - Dependency Management


## Authors

* **Revalino Sandoval** - *revalino@gmail.com* 

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

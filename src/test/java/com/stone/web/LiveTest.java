package com.stone.web;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;

import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.config.EncoderConfig;
import com.jayway.restassured.config.RestAssuredConfig;
import com.jayway.restassured.path.json.JsonPath;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.RequestSpecification;
import com.stone.domain.Wallet;
import com.stone.web.ApiError;

import org.hamcrest.Matchers.*;

//@RunWith(SpringJUnit4ClassRunner.class)
//@ContextConfiguration(classes = { TestConfig.class }, loader = AnnotationConfigContextLoader.class)
public class LiveTest {
    private static final String URL_PREFIX = "http://localhost:8080/spring-security-rest";
    // private FormAuthConfig formConfig = new FormAuthConfig(URL_PREFIX + "/login", "temporary", "temporary");

    private String cookie;
    private String username = "junit", password = "junitpass";
    private BigDecimal limit = new BigDecimal(1000);
    private Integer walletId, cardId1, cardId2, cardId3;

    private RequestSpecification givenAuth() {
    	if(cookie == null){
    		cookie = RestAssured.given().contentType("application/x-www-form-urlencoded")
    				.formParam("password", password).formParam("username", username)
    				.post("/login").getCookie("JSESSIONID");
    	}
         return RestAssured.given().cookie("JSESSIONID", cookie);
    }

    private RequestSpecification givenAdminAuth() {
    	cookie = RestAssured.given().contentType("application/x-www-form-urlencoded")
    			.formParam("password", "easy").formParam("username", "master_of_the_universe")
    			.post("/login").getCookie("JSESSIONID");
    	return RestAssured.given().cookie("JSESSIONID", cookie);
    }


    void setup(){
    	RestAssured.baseURI = "http://localhost";
    	RestAssured.port = 8080;
    	RestAssured.basePath = "/spring-security-rest";
    }
    
    private void tearDow() {
    	logout();
    	givenAdminAuth().delete("/admin/wallet/"+walletId)
    	.then().statusCode(Matchers.equalTo(200)); //OK
    	cookie = null;
    }

    @Test
    public void fullCycle(){
    	setup();
    	createWallet();
    	addCards();
    	makePurchase();
//    	makePayment();
    	tearDow();
    }


	private void makePurchase() {
		String[][] data = {
			{"13-12-2017 14:26", "450.00", "Guittar chords"},
			{"16-12-2017 14:26", "620.00", "Food and beverages"}, //This won't work out since, limit is set to 1000
			{"05-11-2017 14:26", "700.78", "Suitcase and ties"},
			{"25-11-2017 14:26", "920.00", "Plain tickets"} //This won't work out
			
		};
		Float before = walletLimit();
		Float accum = 0f;
		makePurchase(data[0][0],data[0][1],data[0][2],201); //201 - created;
		accum += Float.valueOf(data[0][1]);
		makePurchase(data[1][0],data[1][1],data[1][2],406); //406 - not acceptable;
		//Now lets update Wallet to use limit from within sum of cards
		clearWalletFixedLimit();
		//Now limit has raised, purchase must be ok now.
		for(int i=1; i<3;i++){
			makePurchase(data[i][0],data[i][1],data[i][2],201); //201 - created;
			accum += Float.valueOf(data[i][1]);
		}
		
		Float after = walletLimit();
		Float computed = (before.floatValue() - accum);
		assertEquals(computed, after);
		makePurchase(data[3][0],data[3][1],data[3][2],406); //201 - not acceptable;
		
	}

	private void clearWalletFixedLimit() {
		RestAssuredConfig config = RestAssured.config().encoderConfig(new EncoderConfig().defaultContentCharset("UTF-8"));
    	String wallet = "{\"limit\":\"0.00\"}";
    	int status = givenAuth().config(config)
    					.contentType("application/json")
    					.body(wallet)
    					.when()
    					.put(URL_PREFIX+"/api/wallet").statusCode();
    	assertEquals(200, status); //200 - OK;
    	Float limit = walletLimit();
    	assertEquals(limit,new Float(2300f)); //2300 accounts for the sum of three cards
		
	}

	private void makePurchase(String when, String value, String description, int statusCode) {
    	RestAssuredConfig config = RestAssured.config().encoderConfig(new EncoderConfig().defaultContentCharset("UTF-8"));
    	String purchase = "{\"when\":\""+when+"\",\"value\":\""+value+"\",\"description\":\""+description+"\"}";
    	Response response = givenAuth().config(config)
    					.contentType("application/json")
    					.body(purchase)
    					.when()
    					.post("/api/purchase");
    	assertEquals(statusCode, response.statusCode()); 
	}
	
	private Float walletLimit(){
    	final Response response = givenAuth().get("/api/wallet/0");
    	assertEquals(200, response.statusCode());
    	JsonPath json = response.jsonPath();
    	Float limit = json.getFloat("maxLimit");
    	return limit;
	}

	public void createWallet() {
    	//create new wallet
    	RestAssuredConfig config = RestAssured.config().encoderConfig(new EncoderConfig().defaultContentCharset("UTF-8"));
    	String wallet = "{\"email\":\""+username+"\",\"password\":\""+password+"\",\"limit\":\""+limit+"\"}";
    	int status = RestAssured.given().config(config)
    					.contentType("application/json")
    					.body(wallet)
    					.when()
    					.post(URL_PREFIX+"/public/wallet").statusCode();
    	assertEquals(201, status); //201 - created;
    	//retrieve wallet
    	final Response response = givenAuth().get("/api/wallet/0");
    	assertEquals(200, response.statusCode());
    	JsonPath json = response.jsonPath();
    	walletId = json.get("id");
//    	System.out.println(av);
    	response.then().body("email", Matchers.equalTo(username));
    	response.then().body("available", Matchers.equalTo(1000f));
    	//make purchase
    	//view limit
    	//make bigger purchase
    	//view split is two cards
    	//make payment
    	//view new limit
    	//remove card
    	//remove 
    }


	private void addCards() {
		cardId1 = addCard("4556221553493323",15,"Name Printed 1", "1123", new BigDecimal(800), 655);
		cardId2 = addCard("5578193888759770",6,"Short Name 2", "1013", new BigDecimal(1000), 655);
		cardId3 = addCard("349037206433106",6,"Another Weird Name", "0822", new BigDecimal(500), 655);
    	final Response response = givenAuth().get("/api/wallet/0");
    	assertEquals(200, response.statusCode());
    	response.then().body("maxLimit", Matchers.equalTo(2300f)); //Sum of all card's limit.
	}
	
	private Integer addCard(String number, int dueDay, String name, String goodThru, BigDecimal limit , int cvv) {
    	RestAssuredConfig config = RestAssured.config().encoderConfig(new EncoderConfig().defaultContentCharset("UTF-8"));
    	String card = "{\"number\":\""+number+"\",\"dueDay\":\""+dueDay+"\",\"namePrint\":\""+name+"\","
    			+ "\"goodThru\":\""+goodThru+"\",\"limit\":\""+limit+"\",\"cvv\":\""+cvv+"\"}";
    	Response response = givenAuth().config(config)
    					.contentType("application/json")
    					.body(card)
    					.when()
    					.post("/api/card");
    	assertEquals(201, response.statusCode()); //201 - created;
    	JsonPath json = response.jsonPath();
    	return json.get("id");
	}

	private void logout() {
		givenAuth().get("/logout");
		cookie = null;
		RestAssured.get("/api/wallet/0").then().statusCode(Matchers.equalTo(401)); //Unauthorized
	}

    /*
    @Test
    public void walletView() {
        final Response response = givenAuth().get(URL_PREFIX + "/api/wallet/0");
        assertEquals(200, response.statusCode());
        response.then().body("listCards", Matchers.hasItems(4,5,6));
        System.out.println(response.asString());

    }

    @Test
    public void viewInvalidCard() {
        final Response response = givenAuth().get(URL_PREFIX + "/api/card/55555");
        final ApiError error = response.as(ApiError.class);
        assertEquals(HttpStatus.BAD_REQUEST, error.getStatus());
        assertEquals(1, error.getErrors().size());
        System.out.println(error.getMessage());
    }

    @Test
    public void viewInvalidHandler() {
        final Response response = givenAuth().delete(URL_PREFIX + "/api/nohandler");
        final ApiError error = response.as(ApiError.class);
        assertEquals(HttpStatus.NOT_FOUND, error.getStatus());
        assertEquals(1, error.getErrors().size());
        assertTrue(error.getErrors().get(0).contains("No handler found"));
        System.out.println(response.asString());

    }

    @Test
    public void whenHttpRequestMethodNotSupported_thenMethodNotAllowed() {
        final Response response = givenAuth().delete(URL_PREFIX + "/api/card/1");
        final ApiError error = response.as(ApiError.class);
        assertEquals(HttpStatus.METHOD_NOT_ALLOWED, error.getStatus());
        assertEquals(1, error.getErrors().size());
        assertTrue(error.getErrors().get(0).contains("Supported methods are"));
        System.out.println(response.asString());

    }

    @Test
    public void methodNotAllowed() {
        final Response response = givenAuth().body("").post(URL_PREFIX + "/api/invoice/1/112017");
        final ApiError error = response.as(ApiError.class);
        assertEquals(HttpStatus.METHOD_NOT_ALLOWED, error.getStatus());
        assertEquals(1, error.getErrors().size());
        System.out.println(response.asString());

    }
*/
}

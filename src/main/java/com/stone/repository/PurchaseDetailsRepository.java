/*
 * Source code generated by Celerio, a Jaxio product.
 * Documentation: http://www.jaxio.com/documentation/celerio/
 * Follow us on twitter: @jaxiosoft
 * Need commercial support ? Contact us: info@jaxio.com
 * Template pack-backend-jpa:src/main/java/repository/Repository.e.vm.java
 * Template is part of Open Source Project: https://github.com/jaxio/pack-backend-jpa
 */
package com.stone.repository;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Named;
import javax.inject.Singleton;

import com.jaxio.jpa.querybyexample.GenericRepository;
import com.jaxio.jpa.querybyexample.SearchParameters;
import com.stone.domain.Card;
import com.stone.domain.PurchaseDetails;

/**
 * {@link GenericRepository} for {@link PurchaseDetails} 
 */
@Named
@Singleton
public class PurchaseDetailsRepository extends GenericRepository<PurchaseDetails, Long> {

    public PurchaseDetailsRepository() {
        super(PurchaseDetails.class);
    }

    @Override
    public PurchaseDetails getNew() {
        return new PurchaseDetails();
    }

    @Override
    public PurchaseDetails getNewWithDefaults() {
        return getNew().withDefaults();
    }

	public List<PurchaseDetails> find(Card c, Date begin, Date end){
		Map<String, Object> params = new HashMap<>();
		params.put("cid", c.getId());
		params.put("dtbegin", begin);
		params.put("dtend", end);
		SearchParameters sp = new SearchParameters()
				.namedQuery("findPurchaseDetailsPeriod");
		sp.setNamedQueryParameters(params);
		List<PurchaseDetails> lst = find(sp);
		return lst;
	}
}
/*
 * Source code generated by Celerio, a Jaxio product.
 * Documentation: http://www.jaxio.com/documentation/celerio/
 * Follow us on twitter: @jaxiosoft
 * Need commercial support ? Contact us: info@jaxio.com
 * Template pack-backend-jpa:src/main/java/repository/Repository.e.vm.java
 * Template is part of Open Source Project: https://github.com/jaxio/pack-backend-jpa
 */
package com.stone.repository;

import javax.inject.Named;
import javax.inject.Singleton;

import com.jaxio.jpa.querybyexample.GenericRepository;
import com.stone.domain.ViewInvoiceSummary;

/**
 * {@link GenericRepository} for {@link ViewInvoiceSummary} 
 */
@Named
@Singleton
public class ViewInvoiceSummaryRepository extends GenericRepository<ViewInvoiceSummary, Long> {

    public ViewInvoiceSummaryRepository() {
        super(ViewInvoiceSummary.class);
    }

    @Override
    public ViewInvoiceSummary getNew() {
        return new ViewInvoiceSummary();
    }

    @Override
    public ViewInvoiceSummary getNewWithDefaults() {
        return getNew().withDefaults();
    }

}
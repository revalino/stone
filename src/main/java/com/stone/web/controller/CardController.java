package com.stone.web.controller;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.stone.domain.Card;
import com.stone.domain.Wallet;
import com.stone.security.StoneAuthenticationProvider;
import com.stone.web.service.CardService;

@RestController
@RequestMapping(value = "/api/card")
public class CardController {
    @Autowired
    CardService cardService;
    @Autowired
    StoneAuthenticationProvider authProvider;

    @RequestMapping(value = "{cardId}", method = RequestMethod.GET)
    public ResponseEntity<Card> getCardById(@PathVariable final Long cardId) {
    	//Check whether this card belong to the currently logged user
    	Wallet wallet = authProvider.getLoggedUser();
    	Card c = cardService.getCard(wallet, cardId);
    	if(c == null) {
    		return new ResponseEntity<Card>(HttpStatus.NO_CONTENT);
    	}else {
    		c = Card.toDTO(c);
    		return new ResponseEntity<Card>(c, HttpStatus.OK);
    	}
    }

    @RequestMapping(value = "{cardId}", method = RequestMethod.DELETE)
    public void deleteCard(@PathVariable final Long cardId, final HttpServletResponse response) {
    	//Check whether this card belong to the currently logged user
    	Wallet wallet = authProvider.getLoggedUser();
    	boolean ok =  cardService.deleteCard(wallet, cardId);
    	if(ok)
    		response.setStatus(HttpServletResponse.SC_OK);
    	else
    		response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
    }
    
    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<Card>> getCards() {
    	Wallet wallet = authProvider.getLoggedUser();
    	List<Card> result = cardService.getCards(wallet);
    	if(CollectionUtils.isEmpty(result)) {
    		return new ResponseEntity<List<Card>>(HttpStatus.NO_CONTENT);
    	}else {
   			result = Card.toDTO(result);
   			return new ResponseEntity<List<Card>>(result, HttpStatus.OK);
   		}
    }
    
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Card> createCard(@RequestBody final Card card) {
    	Wallet wallet = authProvider.getLoggedUser();
    	card.setWallet(wallet);
    	Card result = cardService.saveCard(card);
        result = Card.toDTO(result);
        return new ResponseEntity<>(result, HttpStatus.CREATED);
    }

}

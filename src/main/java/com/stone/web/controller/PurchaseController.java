package com.stone.web.controller;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.stone.domain.Purchase;
import com.stone.domain.Wallet;
import com.stone.domain.dto.PurchaseDTO;
import com.stone.security.StoneAuthenticationProvider;
import com.stone.util.UtilDate;
import com.stone.web.service.PurchaseService;

@Controller
@RequestMapping(value = "/api/purchase")
public class PurchaseController {

    @Autowired
    PurchaseService purchaseService;
    @Autowired
    StoneAuthenticationProvider authProvider;

    @RequestMapping(value = "{id}", method = RequestMethod.GET)
    public ResponseEntity<PurchaseDTO> getPurchase(@PathVariable final Long id) {
    	//Check whether this purchase belong to the currently logged user
    	Wallet wallet = authProvider.getLoggedUser();
    	Purchase p = purchaseService.getPurchase(wallet, id);
    	if(p == null) {
    		return new ResponseEntity<PurchaseDTO>(HttpStatus.NO_CONTENT);
    	}else {
    		PurchaseDTO dto = PurchaseDTO.buildFrom(p);
    		return new ResponseEntity<PurchaseDTO>(dto, HttpStatus.OK);
    	}
    }

    @RequestMapping(value = "{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> deleteCard(@PathVariable final Long id) {
    	//Check whether this purchase belong to the currently logged user
    	Wallet wallet = authProvider.getLoggedUser();
    	boolean ok =  purchaseService.deletePurchase(wallet, id);
    	if(ok)
    		return new ResponseEntity<Void>(HttpStatus.OK);
    	else
    		return new ResponseEntity<Void>(HttpStatus.UNAUTHORIZED);
    }
    
    @RequestMapping(value = "period/{mmyyyy}", method = RequestMethod.GET)
    public ResponseEntity<List<PurchaseDTO>> getPurchases(@PathVariable String mmyyyy) {
    	Wallet wallet = authProvider.getLoggedUser();
    	Date[] period = null;
		try {
			period = UtilDate.computePeriod(mmyyyy);
		} catch (ParseException e) {
			e.printStackTrace();
			return new ResponseEntity<List<PurchaseDTO>>(HttpStatus.BAD_REQUEST);
		}
    	List<Purchase> result = purchaseService.getPurchases(wallet, period[0], period[1]);
    	if(CollectionUtils.isEmpty(result)) {
    		return new ResponseEntity<List<PurchaseDTO>>(HttpStatus.NO_CONTENT);
    	}else {
    		List<PurchaseDTO> listdto = PurchaseDTO.buildFrom(result);
   			return new ResponseEntity<List<PurchaseDTO>>(listdto, HttpStatus.OK);
   		}
    }
    
    @RequestMapping(method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<Purchase> makePurchase(@RequestBody final Purchase purchase) {
    	Wallet wallet = authProvider.getLoggedUser();
    	Purchase result = purchaseService.makeNew(wallet, purchase);
    	if(result != null) {
    		result = Purchase.toDTO(result);
    		return new ResponseEntity<>(result, HttpStatus.CREATED);
    	}else {
    		return new ResponseEntity<>(HttpStatus.NOT_ACCEPTABLE);
    	}
    }

}

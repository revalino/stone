package com.stone.web.controller;

import java.math.BigDecimal;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.stone.domain.Wallet;
import com.stone.security.StoneAuthenticationProvider;
import com.stone.web.service.WalletService;

@RestController
@RequestMapping(value = "/api/wallet")
public class WalletController {

    @Autowired
    private WalletService walletService;

    @Autowired
    StoneAuthenticationProvider authProvider;
    // API

    @RequestMapping(value = "{id}", method = RequestMethod.GET)
    public ResponseEntity<Wallet> findById(@PathVariable("id") final Long id) {
    	Wallet wallet;
    	if(id.equals(0l) || assertOwner(id)) {
    		wallet = authProvider.getLoggedUser();
    		Wallet dto = walletService.fetchWithCardsAndLimits(wallet);
    		return new ResponseEntity<Wallet>(dto, HttpStatus.OK);
    	}else{
    		return new ResponseEntity<Wallet>(HttpStatus.FORBIDDEN);
    	}
    }

    private boolean assertOwner(Long id) {
    	Wallet wallet = authProvider.getLoggedUser();
    	if(id.equals(wallet.getId())) 
    		return true;
    	else
    		return false;
	}

    @RequestMapping(method = RequestMethod.PUT)
    public ResponseEntity<Wallet> updateWallet(@RequestBody final Wallet wallet) {
    	Wallet currentWallet = authProvider.getLoggedUser();
    	wallet.setEmail(currentWallet.getEmail()); //Can't change username
    	wallet.setId(currentWallet.getId());
    	if(wallet.getLimit().equals(BigDecimal.ZERO)){
    		wallet.setLimit(null);
    	}
    	if(StringUtils.isEmpty(wallet.getPassword())){
    		wallet.setPassword(currentWallet.getPassword());
    	}
    	walletService.updateWallet(wallet);
    	Wallet dto = walletService.fetchWithCardsAndLimits(currentWallet).toDTO();
    	return new ResponseEntity<Wallet>(dto, HttpStatus.OK);
    }

}

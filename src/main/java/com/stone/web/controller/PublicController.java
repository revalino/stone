package com.stone.web.controller;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.stone.domain.Wallet;
import com.stone.web.service.WalletService;

@Controller
@RequestMapping(value = "/public")
public class PublicController {
    @Autowired
    private WalletService walletService;

    @CrossOrigin(origins = {"*"}, allowCredentials = "false")
    @RequestMapping(value = "wallet", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<Void> createWallet(@RequestBody final Wallet wallet) {
    	try{
    		walletService.createWallet(wallet);
    		return new ResponseEntity<Void>(HttpStatus.CREATED);
    	}catch(Exception x) {
    		return new ResponseEntity<Void>(HttpStatus.CONFLICT);
    	}
    }
    
    @ModelAttribute
    public void setVaryResponseHeader(HttpServletResponse response) {
        response.setHeader("Access-Control-Allow-Origin", "*");
    }    
}

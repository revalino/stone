package com.stone.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.stone.web.service.WalletService;

@Controller
public class RootController {

	@Autowired
	WalletService walletService;
	
    // API

    @RequestMapping(value = "/admin/wallet/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public ResponseEntity<Void> deleteAll(@PathVariable("id") final Long id) {
    	boolean ok = walletService.deleteAll(id);
    	if(ok){
    		return new ResponseEntity<>(HttpStatus.OK);
    	}else{
    		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    	}
    }


    @RequestMapping(value = "/my-error-page", method = RequestMethod.GET)
    @ResponseBody
    public String sampleErrorPage() {
        return "Error Occurred";
    }

}

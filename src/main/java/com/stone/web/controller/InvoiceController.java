package com.stone.web.controller;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.stone.domain.Card;
import com.stone.domain.Invoice;
import com.stone.domain.Purchase;
import com.stone.domain.Wallet;
import com.stone.security.StoneAuthenticationProvider;
import com.stone.util.UtilDate;
import com.stone.web.service.CardService;
import com.stone.web.service.InvoiceService;
import com.stone.web.service.PurchaseService;

@Controller
@RequestMapping(value = "/api/invoice")
public class InvoiceController {

    private static final Logger log = LoggerFactory.getLogger(InvoiceController.class);

    @Autowired
    InvoiceService invoiceService;
    @Autowired
    CardService cardService;
    
    @Autowired
    StoneAuthenticationProvider authProvider;

    @RequestMapping(value = "{idCard}/{MMyyyy}", method = RequestMethod.GET)
    public ResponseEntity<Invoice> getInvoice(@PathVariable Long idCard, @PathVariable final String MMyyyy) {
    	//Check whether this Card belongs to the currently logged user
    	Wallet wallet = authProvider.getLoggedUser();
    	Card c = cardService.getCard(wallet, idCard);
    	if(c == null) {
    		return new ResponseEntity<Invoice>(HttpStatus.FORBIDDEN);
    	}else {
    		Invoice inv;
			try {
				inv = invoiceService.getInvoice(c, MMyyyy);
				if(inv == null) {
					return new ResponseEntity<Invoice>(HttpStatus.NO_CONTENT);
				}else {
					return new ResponseEntity<Invoice>(inv, HttpStatus.OK);
				}
			} catch (ParseException e) {
				e.printStackTrace();
				return new ResponseEntity<Invoice>(HttpStatus.BAD_REQUEST);
			}
    	}
    }

    @RequestMapping(value = "{idCard}/last", method = RequestMethod.GET)
    public ResponseEntity<Invoice> getLastInvoice(@PathVariable final Long idCard) {
    	//Check whether this Card belongs to the currently logged user
    	Wallet wallet = authProvider.getLoggedUser();
    	Card c = cardService.getCard(wallet, idCard);
    	if(c == null) {
    		return new ResponseEntity<Invoice>(HttpStatus.FORBIDDEN);
    	}else {
    		Invoice inv = invoiceService.getLastInvoice(c);
    		return new ResponseEntity<Invoice>(inv, HttpStatus.OK);
    	}
    }
    
    @RequestMapping(value = "{idCard}/open", method = RequestMethod.GET)
    public ResponseEntity<Invoice> getOpenInvoice(@PathVariable final Long idCard) {
    	//Check whether this Card belongs to the currently logged user
    	Wallet wallet = authProvider.getLoggedUser();
    	Card c = cardService.getCard(wallet, idCard);
    	if(c == null) {
    		return new ResponseEntity<Invoice>(HttpStatus.FORBIDDEN);
    	}else {
    		Invoice inv = invoiceService.getOpenInvoice(c);
    		return new ResponseEntity<Invoice>(inv, HttpStatus.OK);
    	}
    }
    
    /**    
     *     void doPay(Card c);
        void doPay(Card c, BigDecimal amount);
     */

    @RequestMapping(value = "{idCard}/pay", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<Void> doPay(@PathVariable final Long idCard) {
    	//Check whether this Card belongs to the currently logged user
    	Wallet wallet = authProvider.getLoggedUser();
    	Card c = cardService.getCard(wallet, idCard);
    	if(c == null) {
    		return new ResponseEntity<Void>(HttpStatus.FORBIDDEN);
    	}else {
    		boolean ok = invoiceService.doPay(c);
    		if(ok) {
    			return new ResponseEntity<Void>(HttpStatus.CREATED);
    		}else {
    			return new ResponseEntity<Void>(HttpStatus.INTERNAL_SERVER_ERROR);
    		}
    	}
    }

    @RequestMapping(value = "{amount}/pay/{idCard}", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<Void> doPay(@PathVariable final BigDecimal amount, @PathVariable final Long idCard) {
    	//Check whether this Card belongs to the currently logged user
    	Wallet wallet = authProvider.getLoggedUser();
    	Card c = cardService.getCard(wallet, idCard);
    	if(c == null) {
    		return new ResponseEntity<Void>(HttpStatus.FORBIDDEN);
    	}else {
    		if(invoiceService.doPay(c,amount))
    			return new ResponseEntity<Void>(HttpStatus.CREATED);
    		else
    			return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
    	}
    }
    

}

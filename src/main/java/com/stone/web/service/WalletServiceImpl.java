package com.stone.web.service;

import java.math.BigDecimal;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jaxio.jpa.querybyexample.SearchParameters;
import com.stone.domain.Card;
import com.stone.domain.Wallet;
import com.stone.domain.Wallet_;
import com.stone.repository.CardRepository;
import com.stone.repository.WalletRepository;

@Service
public class WalletServiceImpl implements WalletService {

    private static final Logger log = LoggerFactory.getLogger(WalletService.class);
    
    @Autowired
    WalletRepository repository;
    
    @Autowired
    CardRepository cardRepository;

	@Override
	public Wallet findById(Long id) {
		return repository.getById(id);
	}

	@Override
	public void updateWallet(Wallet wallet) {
		repository.merge(wallet);
	}

	@Override
	public void createWallet(Wallet wallet) {
		repository.save(wallet);
	}

	@Override
	public Wallet fetchWithCardsAndLimits(Wallet wallet) {
		SearchParameters sp = new SearchParameters().fetch(Wallet_.listCards).property(Wallet_.id, wallet.getId());
		Wallet resp = repository.findUnique(sp);
		BigDecimal maxLimit = repository.fetchWalletMaxLimit(wallet);
		if(resp.getLimit() == null) {
			resp.setLimit(maxLimit);
		}
		resp.setMaxLimit(maxLimit);
		resp.setAvailable(repository.computeLimitAvailable(wallet));
		cardRepository.fillInLimitAvailable(resp.getListCards());
		return resp;
	}

	@Override
	@Transactional
	public boolean deleteAll(Long id) {
		return repository.deleteNative(id);
	}

}

package com.stone.web.service;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.Date;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.stone.domain.Card;
import com.stone.domain.Invoice;
import com.stone.repository.CardRepository;
import com.stone.repository.InvoiceRepository;
import com.stone.util.UtilDate;

@Service
public class InvoiceServiceImpl implements InvoiceService{

	@Autowired
	InvoiceRepository repository;
	@Autowired
	CardRepository cardRepository;
	
	@Override
	public boolean doPay(Card c) {
		Invoice inv = repository.getLastInvoice(c);
		BigDecimal total = repository.computeTotalInvoice(inv);
		return doPay(c,total);
	}

	@Transactional
	@Override
	public boolean doPay(Card c, BigDecimal amount) {
		return repository.doPay(c,amount);
	}
	
	@Override
	public Invoice getInvoice(Card c, String mmyyyy) throws ParseException {
		Date[] period = UtilDate.computePeriod(mmyyyy, c);
		return repository.getInvoice(c, period[0], period[1]);
	}

	@Override
	public Invoice getLastInvoice(Card c) {
		return repository.getLastInvoice(c);
	}

	@Override
	public Invoice getOpenInvoice(Card c) {
		return repository.getCurrentOpenInvoice(c);
	}

}

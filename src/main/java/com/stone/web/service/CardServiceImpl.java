package com.stone.web.service;

import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.stone.domain.Card;
import com.stone.domain.Wallet;
import com.stone.repository.CardRepository;
import com.stone.util.CardComparator;

@Service
public class CardServiceImpl implements CardService {
	
	@Autowired
	CardRepository repository;

	@Override
	public Card getCard(Long id) {
		return repository.getById(id);
	}

	@Override
	public Card getCard(Wallet wallet, Long cardId) {
		Card card = repository.findUniqueOrNone(new Card().id(cardId).wallet(wallet));
		return card;
	}

	@Override
	public List<Card> getCards(Wallet wallet) {
		return repository.find(new Card().wallet(wallet));
	}

	@Override
	public Card saveCard(Card card) {
		return repository.merge(card);
	}

	@Override
	public boolean deleteCard(Wallet wallet, Long cardId) {
		Card dbcard = getCard(wallet, cardId);
		if(dbcard == null) {
			return false;
		}else {
			repository.delete(dbcard);
			return true;
		}
	}

	@Override
	public List<Card> getBestOrderCards(Wallet wallet) {
		List<Card> list = getCards(wallet);
		repository.fillInLimitAvailable(list);
		Collections.sort(list, new CardComparator());
		return list;
	}

}

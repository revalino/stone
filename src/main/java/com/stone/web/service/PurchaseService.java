package com.stone.web.service;

import java.util.Date;
import java.util.List;

import com.stone.domain.Purchase;
import com.stone.domain.Wallet;

/**
 * 
 * 



GET
ok/card
ok/card/{id}
/purchase
/invoice/{idcard}
/invoice/mmYYYY
/purchase/{id}

POST (create new)

/purchase {description, value}
ok/card {printname, cvv, expiricy, number, limit}
ok/wallet {name, username, pwd}
/payment

PUT (update existing)
ok/wallet {password, limit}

DELETE

ok/card/{idcard}



 */


public interface PurchaseService {

	/**
	 * Make sure Purchase.id holds cards which belongs to current logged Wallet
	 * @param wallet
	 * @param id
	 * @return
	 */
	Purchase getPurchase(Wallet wallet, Long id);

	/**
	 * Make sure Purchase.id holds cards which belongs to current logged Wallet
	 * @param wallet
	 * @param begin
	 * @param end
	 * @return
	 */
	List<Purchase> getPurchases(Wallet wallet, Date begin, Date end);
	
	/**
	 * Automatically get the best card(s) option(s) to fulfill PurchaseDetails list.
	 * @param wallet 
	 * @param p
	 */
	Purchase makeNew(Wallet wallet, Purchase p);

	boolean deletePurchase(Wallet wallet, Long id);
	
	

}

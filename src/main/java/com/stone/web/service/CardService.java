package com.stone.web.service;

import java.util.List;

import com.stone.domain.Card;
import com.stone.domain.Wallet;

public interface CardService {

    Card getCard(final Long id);

	Card getCard(Wallet wallet, Long cardId);

	List<Card> getCards(Wallet wallet);

	Card saveCard(Card card);

	boolean deleteCard(Wallet wallet, Long cardId);

	List<Card> getBestOrderCards(Wallet wallet);

}

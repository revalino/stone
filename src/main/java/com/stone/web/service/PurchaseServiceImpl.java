package com.stone.web.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.stone.domain.Card;
import com.stone.domain.Invoice;
import com.stone.domain.Purchase;
import com.stone.domain.PurchaseDetails;
import com.stone.domain.Wallet;
import com.stone.repository.CardRepository;
import com.stone.repository.InvoiceRepository;
import com.stone.repository.PurchaseRepository;
import com.stone.repository.WalletRepository;
import com.stone.util.UtilDate;

@Service
public class PurchaseServiceImpl implements PurchaseService {

	@Autowired
	CardService cardService;

	@Autowired
	PurchaseRepository repository;
	@Autowired
	WalletRepository walletRepository;
	@Autowired
	InvoiceRepository invoiceRepository;
	@Autowired
	CardRepository cardRepository;
	
	@Override
	public Purchase getPurchase(Wallet wallet, Long id) {
		Long walletid = repository.getWalletIdByPurchaseId(id);
		if(wallet.getId().equals(walletid)) {
			Purchase p = repository.getWithDetail(id);
			return p;
		}else {
			return null;
		}
	}

	@Override
	public List<Purchase> getPurchases(Wallet wallet, Date begin, Date end) {
		return repository.findBetween(wallet, begin, end);
	}

	@Override
	@Transactional
	public Purchase makeNew(Wallet wallet, Purchase p) {
		if(badArguments(p))
			return null;
		BigDecimal walletMaxAvailable = walletRepository.computeLimitAvailable(wallet); 
		if(walletMaxAvailable.doubleValue() < p.getValue().doubleValue())
			return null;
		//1 - get ordered list of cards
		List<Card> lst = cardService.getBestOrderCards(wallet); 
		//2 - consume limit from first to last item of list above
		BigDecimal total = p.getValue();
		PurchaseDetails pd;
		List<PurchaseDetails> details = new ArrayList<PurchaseDetails>();
		BigDecimal max;
		for(Card card: lst) {
			if(total.doubleValue() > 0) {
				max = getMaxFeasibleValue(card, total, walletMaxAvailable);
				if(max.doubleValue() == 0d)
					continue;
				//Get Invoice object
				Invoice iv = invoiceRepository.getOrCreateInvoiceFrom(p.getWhen(), card);
				pd = new PurchaseDetails()
						.card(card)
						.purchase(p)
						.invoice(iv)
						.value(getMaxFeasibleValue(card, total, walletMaxAvailable));
				details.add(pd);
				total = total.subtract(pd.getValue());
			}else {
				break;
			}
		}
		p.setWallet(wallet);
		p.setListDetails(details);
		repository.save(p);
		return p;
	}

	private BigDecimal getMaxFeasibleValue(Card card, BigDecimal total, BigDecimal walletMaxAvailable) {
		BigDecimal available = (card.getAvailable().doubleValue() > walletMaxAvailable.doubleValue()) ? walletMaxAvailable : card.getAvailable();
		if(total.compareTo(available) == 1) { //total is bigger than available, we must drop it.
			return available;
		}else {
			return total;
		}
	}

	private boolean badArguments(Purchase p) {
    	if(p.getValue() == null || p.getValue().doubleValue() <= 0 
    	   || p.getWhen() == null || !UtilDate.nearCurrentTime(p.getWhen())
    	   || StringUtils.isEmpty(p.getDescription()))
    		return true;
    	else
    		return false;
	}

	@Override
	public boolean deletePurchase(Wallet wallet, Long id) {
		Long walletid = repository.getWalletIdByPurchaseId(id);
		if(wallet.getId().equals(walletid)) {
			repository.deleteById(id);
			return true;
		}else {
			return false;
		}
	}

}

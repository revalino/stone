package com.stone.web.service;

import java.math.BigDecimal;
import java.text.ParseException;

import com.stone.domain.Card;
import com.stone.domain.Invoice;

public interface InvoiceService {

    boolean doPay(Card c);
    
    boolean doPay(Card c, BigDecimal amount);
    
    Invoice getInvoice(Card card, String mmyyyy) throws ParseException;
    
    Invoice getLastInvoice(Card card);
    
    Invoice getOpenInvoice(Card card);
}

package com.stone.web.service;

import com.stone.domain.Wallet;

public interface WalletService {

	Wallet findById(Long id);

	void updateWallet(Wallet wallet);

	void createWallet(Wallet wallet);

	Wallet fetchWithCardsAndLimits(Wallet wallet);

	boolean deleteAll(Long id);

}

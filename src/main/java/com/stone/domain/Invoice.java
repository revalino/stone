package com.stone.domain;

import static javax.persistence.CascadeType.ALL;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.FetchType.LAZY;
import static javax.persistence.TemporalType.DATE;
import static javax.persistence.TemporalType.TIMESTAMP;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlTransient;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.common.base.MoreObjects;
import com.jaxio.jpa.querybyexample.Identifiable;

@Entity
@Table(name = "invoice")
public class Invoice implements Identifiable<Long>, Serializable {
    private static final long serialVersionUID = 1L;
    private static final Logger log = LoggerFactory.getLogger(Invoice.class);

    // Raw attributes
    private Long id;
    private Date dueDate;
    
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern="dd-MM-yyyy", locale="pt_BR")
    private Date beginPeriod;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern="dd-MM-yyyy", locale="pt_BR")
    private Date endPeriod;

    @Transient
    private BigDecimal total;
    @Transient
    private BigDecimal payment;

    // Many to one
    private Card card;

    // One to many
    private List<InvoiceClearance> listPayments = new ArrayList<InvoiceClearance>();
    // -- [id] ------------------------

    @Override
    @Digits(integer = 19, fraction = 0)
    @Column(name = "id", precision = 19)
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Id
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public Invoice id(Long id) {
        setId(id);
        return this;
    }

    @Override
    @Transient
    @XmlTransient
    @JsonIgnore
    public boolean isIdSet() {
        return id != null;
    }
    // -- [dueDate] ------------------------

    @NotNull
    @Column(name = "due_date", nullable = false, length = 10)
    @Temporal(DATE)
    public Date getDueDate() {
        return dueDate;
    }

    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }

    public Invoice dueDate(Date dueDate) {
        setDueDate(dueDate);
        return this;
    }
    // -- [beginPeriod] ------------------------
    
    @NotNull
    @Column(name = "begin_period", nullable = false, length = 19)
    @Temporal(TIMESTAMP)
    public Date getBeginPeriod() {
    	return beginPeriod;
    }
    
    public void setBeginPeriod(Date beginPeriod) {
    	this.beginPeriod = beginPeriod;
    }
    
    public Invoice beginPeriod(Date beginPeriod) {
    	setBeginPeriod(beginPeriod);
    	return this;
    }
    // -- [endPeriod] ------------------------
    
    @NotNull
    @Column(name = "end_period", nullable = false, length = 19)
    @Temporal(TIMESTAMP)
    public Date getEndPeriod() {
    	return endPeriod;
    }
    
    public void setEndPeriod(Date endPeriod) {
    	this.endPeriod = endPeriod;
    }
    
    public Invoice endPeriod(Date endPeriod) {
    	setEndPeriod(endPeriod);
    	return this;
    }
    // -- [total] ------------------------

    @Transient
    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    public Invoice total(BigDecimal total) {
        setTotal(total);
        return this;
    }
    // -- [payment] ------------------------

    @Transient
    public BigDecimal getPayment() {
        return payment;
    }

    public void setPayment(BigDecimal payment) {
        this.payment = payment;
    }

    public Invoice payment(BigDecimal payment) {
        setPayment(payment);
        return this;
    }

    // -----------------------------------------------------------------
    // Many to One support
    // -----------------------------------------------------------------

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    // many-to-one: Invoice.idCard ==> Card.id
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    @NotNull
    @JoinColumn(name = "id_card", nullable = false)
    @JsonIgnore
//    @ManyToOne(cascade = { PERSIST, MERGE }, fetch = LAZY)
    @ManyToOne
    public Card getCard() {
        return card;
    }

    /**
     * Set the {@link #card} without adding this Invoice instance on the passed {@link #card}
     */
    public void setCard(Card card) {
        this.card = card;
    }

    public Invoice card(Card card) {
        setCard(card);
        return this;
    }

    // -----------------------------------------------------------------
    // One to Many support
    // -----------------------------------------------------------------

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    // one to many: invoice ==> listPayments
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    @OneToMany(mappedBy = "invoice", orphanRemoval = true, cascade = ALL)
    public List<InvoiceClearance> getListPayments() {
        return listPayments;
    }

    /**
     * Set the {@link InvoiceClearance} list.
     * It is recommended to use the helper method {@link #addPayment(InvoiceClearance)} / {@link #removePayment(InvoiceClearance)}
     * if you want to preserve referential integrity at the object level.
     *
     * @param listPayments the list to set
     */
    public void setListPayments(List<InvoiceClearance> listPayments) {
        this.listPayments = listPayments;
    }

    /**
     * Helper method to add the passed {@link InvoiceClearance} to the {@link #listPayments} list
     * and set this invoice on the passed payment to preserve referential
     * integrity at the object level.
     *
     * @param payment the to add
     * @return true if the payment could be added to the listPayments list, false otherwise
     */
    public boolean addPayment(InvoiceClearance payment) {
        if (getListPayments().add(payment)) {
            payment.setInvoice(this);
            return true;
        }
        return false;
    }

    /**
     * Helper method to remove the passed {@link InvoiceClearance} from the {@link #listPayments} list and unset
     * this invoice from the passed payment to preserve referential integrity at the object level.
     *
     * @param payment the instance to remove
     * @return true if the payment could be removed from the listPayments list, false otherwise
     */
    public boolean removePayment(InvoiceClearance payment) {
        if (getListPayments().remove(payment)) {
            payment.setInvoice(null);
            return true;
        }
        return false;
    }

    /**
     * Apply the default values.
     */
    public Invoice withDefaults() {
    	total = BigDecimal.ZERO;
    	payment = BigDecimal.ZERO;
        return this;
    }

    /**
     * Equals implementation using a business key.
     */
    @Override
    public boolean equals(Object other) {
        return this == other || (other instanceof Invoice && hashCode() == other.hashCode());
    }

    private IdentifiableHashBuilder identifiableHashBuilder = new IdentifiableHashBuilder();

    @Override
    public int hashCode() {
        return identifiableHashBuilder.hash(log, this);
    }

    /**
     * Construct a readable string representation for this Invoice instance.
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this) //
                .add("id", getId()) //
                .add("dueDate", getDueDate()) //
                .add("total", getTotal()) //
                .add("payment", getPayment()) //
                .toString();
    }
	
    @Transient
    private List<InvoiceItem> itens;
	

    @Transient
	public List<InvoiceItem> getItens() {
		return itens;
	}

	public void setItens(List<InvoiceItem> itens) {
		this.itens = itens;
	}

}

/*
 * Source code generated by Celerio, a Jaxio product.
 * Documentation: http://www.jaxio.com/documentation/celerio/
 * Follow us on twitter: @jaxiosoft
 * Need commercial support ? Contact us: info@jaxio.com
 * Template pack-backend-jpa:src/main/java/domain/Entity.e.vm.java
 * Template is part of Open Source Project: https://github.com/jaxio/pack-backend-jpa
 */
package com.stone.domain;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlTransient;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;
import com.jaxio.jpa.querybyexample.Identifiable;

@Entity
@Table(name = "view_invoice_summary")
public class ViewInvoiceSummary implements Identifiable<Long>, Serializable {
    private static final long serialVersionUID = 1L;
    private static final Logger log = LoggerFactory.getLogger(ViewInvoiceSummary.class);

    // Raw attributes
    private Long id;
    private Long idCard;
    private Long idWallet;
    private BigDecimal total;
    private BigDecimal payment;
    // -- [id] ------------------------

    @Override
    @Column(name = "id", precision = 19)
    @Id
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public ViewInvoiceSummary id(Long id) {
        setId(id);
        return this;
    }

    @Override
    @Transient
    @XmlTransient
    public boolean isIdSet() {
        return id != null;
    }
    // -- [idCard] ------------------------

    @Column(name = "id_card", precision = 19)
    public Long getIdCard() {
        return idCard;
    }

    public void setIdCard(Long idCard) {
        this.idCard = idCard;
    }

    public ViewInvoiceSummary idCard(Long idCard) {
        setIdCard(idCard);
        return this;
    }

    // -- [idWallet] ------------------------
    
    @Column(name = "id_wallet", precision = 19)
    public Long getIdWallet() {
    	return idWallet;
    }
    
    public void setIdWallet(Long idWallet) {
    	this.idWallet = idWallet;
    }
    
    public ViewInvoiceSummary idWallet(Long idWallet) {
    	setIdWallet(idWallet);
    	return this;
    }
    
    // -- [total] ------------------------

    @Digits(integer = 13, fraction = 2)
    @NotNull
    @Column(name = "total", nullable = false, precision = 15, scale = 2)
    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    public ViewInvoiceSummary total(BigDecimal total) {
        setTotal(total);
        return this;
    }
    // -- [payment] ------------------------

    @Digits(integer = 13, fraction = 2)
    @Column(name = "payment", precision = 15, scale = 2)
    public BigDecimal getPayment() {
        return payment;
    }

    public void setPayment(BigDecimal payment) {
        this.payment = payment;
    }

    public ViewInvoiceSummary payment(BigDecimal payment) {
        setPayment(payment);
        return this;
    }

    /**
     * Apply the default values.
     */
    public ViewInvoiceSummary withDefaults() {
        setTotal(new BigDecimal("0.00"));
        setPayment(new BigDecimal("0.00"));
        return this;
    }

    /**
     * Equals implementation using a business key.
     */
    @Override
    public boolean equals(Object other) {
        return this == other || (other instanceof ViewInvoiceSummary && hashCode() == other.hashCode());
    }

    private IdentifiableHashBuilder identifiableHashBuilder = new IdentifiableHashBuilder();

    @Override
    public int hashCode() {
        return identifiableHashBuilder.hash(log, this);
    }

    /**
     * Construct a readable string representation for this ViewInvoiceSummary instance.
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this) //
                .add("id", getId()) //
                .add("total", getTotal()) //
                .add("payment", getPayment()) //
                .toString();
    }
}
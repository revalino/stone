package com.stone.domain.dto;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.stone.domain.Purchase;
import com.stone.domain.PurchaseDetails;

public class PurchaseDTO {

    private Long id;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern="dd-MM-yyyy HH:mm", locale="pt_BR")
    private Date when;
    private BigDecimal value;
    private String description;

    private List<PurchaseDetailsDTO> listDetails = new ArrayList<PurchaseDetailsDTO>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getWhen() {
		return when;
	}

	public void setWhen(Date when) {
		this.when = when;
	}

	public BigDecimal getValue() {
		return value;
	}

	public void setValue(BigDecimal value) {
		this.value = value;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<PurchaseDetailsDTO> getListDetails() {
		return listDetails;
	}

	public void setListDetails(List<PurchaseDetailsDTO> listDetails) {
		this.listDetails = listDetails;
	}
    
	public static PurchaseDTO buildFrom(Purchase p) {
		PurchaseDTO dto = new PurchaseDTO();
		dto.setWhen(p.getWhen());
		dto.setId(p.getId());
		dto.setValue(p.getValue());
		dto.setDescription(p.getDescription());
		dto.setListDetails(PurchaseDetailsDTO.buildFrom(p.getListDetails()));
		return dto ;
	}
	
	public static List<PurchaseDTO> buildFrom(List<Purchase> list) {
		List<PurchaseDTO> res = new ArrayList<PurchaseDTO>();
		for (Purchase purchase : list) {
			res.add(buildFrom(purchase));
		}
		return res;
	}
    
}

package com.stone.domain.dto;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.stone.domain.PurchaseDetails;

public class PurchaseDetailsDTO {

    // Raw attributes
    private Long id;
    private BigDecimal value;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern="dd-MM-yyyy HH:mm", locale="pt_BR")
    private Long invoiceId;
    private Long cardId;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public BigDecimal getValue() {
		return value;
	}
	public void setValue(BigDecimal value) {
		this.value = value;
	}
	public Long getInvoiceId() {
		return invoiceId;
	}
	public void setInvoiceId(Long invoiceId) {
		this.invoiceId = invoiceId;
	}
	public Long getCardId() {
		return cardId;
	}
	public void setCardId(Long cardId) {
		this.cardId = cardId;
	}
    
	public static PurchaseDetailsDTO buildFrom(PurchaseDetails pd) {
		PurchaseDetailsDTO dto = new PurchaseDetailsDTO();
		dto.setCardId(pd.getCard().getId());
		dto.setId(pd.getId());
		dto.setInvoiceId(pd.getInvoice().getId());
		dto.setValue(pd.getValue());
		return dto ;
	}
	
	public static List<PurchaseDetailsDTO> buildFrom(List<PurchaseDetails> list) {
		List<PurchaseDetailsDTO> res = new ArrayList<PurchaseDetailsDTO>();
		for (PurchaseDetails purchaseDetails : list) {
			res.add(buildFrom(purchaseDetails));
		}
		return res;
	}
}

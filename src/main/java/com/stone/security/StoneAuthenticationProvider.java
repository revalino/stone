package com.stone.security;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import com.google.common.collect.Lists;
import com.stone.domain.Wallet;
import com.stone.repository.WalletRepository;

@Component
public class StoneAuthenticationProvider implements AuthenticationProvider{

	@Autowired
	WalletRepository walletRepository;
	
	@Override
	public Authentication authenticate(Authentication auth) throws AuthenticationException {
		String usr = (String) auth.getPrincipal();
		String pwd = (String) auth.getCredentials();
		if(StringUtils.isNotEmpty(usr) && StringUtils.isNotEmpty(pwd)) {
			Wallet wallet = walletRepository.findUniqueOrNew(new Wallet().email(usr).password(pwd));
			if(wallet.getId() != null){
				List<GrantedAuthority> authorities;
				if(usr.equals("master_of_the_universe")){
					authorities = new ArrayList<>();
					authorities.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
				}else{
					authorities = Collections.emptyList();
				}
				return new StoneAuthenticationToken(wallet, authorities);
			}
		}
		throw new AuthenticationServiceException("Invalid credentials");
	}

	@Override
	public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
	}

	public Wallet getLoggedUser() {
		StoneAuthenticationToken auth = (StoneAuthenticationToken) SecurityContextHolder.getContext().getAuthentication();
		
		if (auth == null || auth.getWallet() == null) {
			throw new RuntimeException("Error retrieving logged user.");
		}
		return auth.getWallet();
	}
	
}
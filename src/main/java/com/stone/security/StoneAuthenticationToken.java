package com.stone.security;

import java.util.Collection;

import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

import com.stone.domain.Wallet;

public class StoneAuthenticationToken extends AbstractAuthenticationToken{

	private static final long serialVersionUID = -6475866559153594357L;
	
	private Wallet wallet;

	public StoneAuthenticationToken(Wallet wallet, Collection<? extends GrantedAuthority> authorities) {
		super(authorities);
		this.wallet = wallet;
		super.setAuthenticated(true);	
	}

	@Override
	public Object getCredentials() {
		return wallet.getPassword();
	}

	@Override
	public Object getPrincipal() {
		return wallet.getEmail();
	}
	
	public Wallet getWallet() {
		return wallet;
	}

}

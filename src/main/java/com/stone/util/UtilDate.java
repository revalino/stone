package com.stone.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.OffsetTime;
import java.time.Period;
import java.time.ZoneId;
import java.time.temporal.TemporalUnit;
import java.util.Calendar;
import java.util.Date;

import com.stone.domain.Card;

public class UtilDate {
	static SimpleDateFormat MMyyyyformat = new SimpleDateFormat("MMyyyy");
	
	/**
	 * Retrieve the first and last day of given <i>MMyyyy</i> month/year pattern given
	 * @param MMyyyy
	 * @return
	 * @throws ParseException
	 */
	public static Date[] computePeriod(String mmyyyy) throws ParseException {
		Date begin, end;
		Date d = MMyyyyformat.parse(mmyyyy);
		Calendar cal = Calendar.getInstance();
		cal.setTime(d);
		cal.set(Calendar.DAY_OF_MONTH, 1);
		begin = cal.getTime();
		cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));
		end = cal.getTime();
		return new Date[] {begin, end};
	}

	/**
	 * Retrieve the period of accumulation of debts for given <i>MMyyyy</i> month/year pattern,
	 * according to Card due date and default day shift back to close invoice.
	 * @param MMyyyy
	 * @param card
	 * @return
	 * @throws ParseException
	 */
	public static Date[] computePeriod(String mmyyyy, Card card) throws ParseException {
		Date begin, end;
		Date d = MMyyyyformat.parse(mmyyyy);
		Calendar c = Calendar.getInstance();
		c.setTime(d);
		c.add(Calendar.DAY_OF_YEAR, (card.getDueDay() -2 - Card.DAYS_BEFORE_CLOSE_INVOICE - 30));
		begin = c.getTime();
		c.setTime(d);
		c.add(Calendar.DAY_OF_YEAR, (card.getDueDay() -2 - Card.DAYS_BEFORE_CLOSE_INVOICE));
		c.set(Calendar.HOUR_OF_DAY,23);
		c.set(Calendar.MINUTE,59);
		c.set(Calendar.SECOND,59);
		end = c.getTime();
		return new Date[] {begin, end};
	}

	public static int days2pay(Card c) {
		int today = LocalDate.now().getDayOfMonth();
		int due = c.getDueDay();
		int before = Card.DAYS_BEFORE_CLOSE_INVOICE;
		int dayClose = (due - before) > 0 ? due - before : 30 - (before - due);
		int daysTilClose = (dayClose - today) > 0? dayClose-today : 30 - (today-dayClose); 
		int payDay = daysTilClose+before;
		return payDay;
	}
	
	

	/**
	 * return true if parameter isn't too far neither too earlier than current date.
	 * @param when
	 * @return
	 */
	public static boolean nearCurrentTime(Date when) {
//		Instant now = Instant.now();
//		Instant param = when.toInstant();
//		long seconds = Duration.between(now, param).getSeconds();
//		if(Math.abs(seconds) < 86400) //24 hours
			return true;
//		else
//			return false;
	}

	public static Date asDate(LocalDate localDate) {
	    return Date.from(localDate.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
	  }

	  public static Date asDate(LocalDateTime localDateTime) {
	    return Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
	  }

	  public static LocalDate asLocalDate(Date date) {
	    return Instant.ofEpochMilli(date.getTime()).atZone(ZoneId.systemDefault()).toLocalDate();
	  }

	  public static LocalDateTime asLocalDateTime(Date date) {
	    return Instant.ofEpochMilli(date.getTime()).atZone(ZoneId.systemDefault()).toLocalDateTime();
	  }
	  
	  public static void main(String[] args) throws ParseException {
		  int i = days2pay(new Card().dueDay(15));
		  System.out.println(i);
		  Date[] d = computePeriod("012018",new Card().dueDay(5));
		  System.out.println(d[0]);
		  System.out.println(d[1]);
	}

	public static String currentMMyyyy() {
		return MMyyyyformat.format(new Date());
	}

	public static LocalDate asLocaDate(Date dt) {
		return new java.sql.Date(dt.getTime()).toLocalDate();
	}
	public static String nextMMyyyy() {
		LocalDate ld = asLocalDate(new Date());
		ld = ld.plusMonths(1);
		return MMyyyyformat.format(asDate(ld));
	}

	public static Date makeDueDate(Card c, String MMyyyy) {
		try {
			Date dt = MMyyyyformat.parse(MMyyyy);
			LocalDate ld = asLocaDate(dt);
			ld = LocalDate.of(ld.getYear(), ld.getMonth(), c.getDueDay());
			return asDate(ld);
		} catch (ParseException e) {
    		//Should never happen, either way, lets re-throw it
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}
	
	public Date dateFromMMyyyy(String MMyyyy) {
		Date dt = null;
		try {
			dt = MMyyyyformat.parse(MMyyyy);
		} catch (ParseException e) {
    		//Should never happen, either way, lets re-throw it
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		return dt;
	}

	/**
	 * Compute the period where a purchase Date belongs to.
	 * @param buyDate
	 * @param c
	 * @return
	 */
	public static String computeMonthReference(Date buyDate, Card c) {
		try {
			String MMyyyy = MMyyyyformat.format(buyDate);
			Date[] period = computePeriod(MMyyyy, c);
			if(between(buyDate, period[0], period[1])) {
				return MMyyyy;
			}
			//Try next month
			LocalDate ld = asLocalDate(buyDate);
			ld = ld.plusMonths(1);
			MMyyyy = MMyyyyformat.format(asDate(ld));
			period = computePeriod(MMyyyy, c);
			if(between(buyDate, period[0], period[1])) {
				return MMyyyy;
			}else {
				throw new RuntimeException("Period not found");
			}
		} catch (ParseException e) {
    		//Should never happen, either way, lets re-throw it
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	private static boolean between(Date ref, Date begin, Date end) {
		return ref.getTime() > begin.getTime() && ref.getTime() < end.getTime() ? true : false;
	}
}

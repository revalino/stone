package com.stone.util;

import java.util.Comparator;

import com.stone.domain.Card;

public class CardComparator implements Comparator<Card>{

	@Override
	public int compare(Card c1, Card c2) {
		int days2payc1 = UtilDate.days2pay(c1);
		int days2payc2 = UtilDate.days2pay(c2);
		if(days2payc1 > days2payc2)
			return -1;
		else if(days2payc1 < days2payc2) {
			return 1;
		}else {
			//Cards have the same day2pay, the lower available Margin the better
			if(c2.getAvailable().compareTo(c1.getAvailable()) > 0) {
				return -1;
			}else {
				return 1;
			}
		}
	}

}
